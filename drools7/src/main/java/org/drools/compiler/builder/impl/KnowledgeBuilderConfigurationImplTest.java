package org.drools.compiler.builder.impl;

import org.junit.Test;
import org.kie.internal.builder.conf.DefaultPackageNameOption;
import org.kie.internal.builder.conf.DumpDirOption;
import org.kie.internal.builder.conf.ParallelRulesBuildThresholdOption;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class KnowledgeBuilderConfigurationImplTest {

    @Test
    public void testParallelRulesBuildThresholdConfiguration() {
        try {
            System.getProperties().put(ParallelRulesBuildThresholdOption.PROPERTY_NAME, "20");
            System.getProperties().put(DefaultPackageNameOption.PROPERTY_NAME, "shareniu");
            System.getProperties().put(DumpDirOption.PROPERTY_NAME, "/Users/shareniu/Downloads");
            KnowledgeBuilderConfigurationImpl kbConfigImpl = new KnowledgeBuilderConfigurationImpl();
            assertThat(kbConfigImpl.getParallelRulesBuildThreshold(), is(20));
            String defaultPackageName = kbConfigImpl.getDefaultPackageName();
            assertThat(defaultPackageName,is("shareniu"));
        } finally {
            System.getProperties().remove(ParallelRulesBuildThresholdOption.PROPERTY_NAME);
            System.getProperties().remove(DefaultPackageNameOption.PROPERTY_NAME);
        }
    }

    @Test
    public void testMinusOneParallelRulesBuildThresholdConfiguration() {
        try {
            System.getProperties().put(ParallelRulesBuildThresholdOption.PROPERTY_NAME, "-1");
            KnowledgeBuilderConfigurationImpl kbConfigImpl = new KnowledgeBuilderConfigurationImpl();
            assertThat(kbConfigImpl.getParallelRulesBuildThreshold(), is(-1));
        } finally {
            System.getProperties().remove(ParallelRulesBuildThresholdOption.PROPERTY_NAME);
        }
    }
}
