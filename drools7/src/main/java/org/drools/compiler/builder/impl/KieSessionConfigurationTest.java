package org.drools.compiler.builder.impl;

import org.drools.core.BeliefSystemType;
import org.drools.core.base.accumulators.AverageAccumulateFunction;
import org.drools.core.base.accumulators.MaxAccumulateFunction;
import org.drools.core.base.evaluators.AfterEvaluatorDefinition;
import org.drools.core.base.evaluators.BeforeEvaluatorDefinition;
import org.drools.core.base.evaluators.EvaluatorDefinition;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.BeliefSystemTypeOption;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.rule.AccumulateFunction;
import org.kie.internal.builder.KnowledgeBuilderConfiguration;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.builder.conf.*;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class KieSessionConfigurationTest {

    private KieSessionConfiguration config;

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        config = KieServices.Factory.get().newKieSessionConfiguration();
    }

    @Test
    public void testClockTypeConfiguration() {
        // setting the option using the type safe method
        config.setOption( ClockTypeOption.get("pseudo") );

        // checking the type safe getOption() method
        assertEquals( ClockTypeOption.get("pseudo"),
                config.getOption( ClockTypeOption.class ) );
        // checking the string based getProperty() method
        assertEquals( "pseudo",
                config.getProperty( ClockTypeOption.PROPERTY_NAME ) );

        // setting the options using the string based setProperty() method
        config.setProperty( ClockTypeOption.PROPERTY_NAME,
                "realtime" );

        // checking the type safe getOption() method
        assertEquals( ClockTypeOption.get("realtime"),
                config.getOption( ClockTypeOption.class ) );
        // checking the string based getProperty() method
        assertEquals( "realtime",
                config.getProperty( ClockTypeOption.PROPERTY_NAME ) );
    }


    @Test
    public void testBeliefSystemType() {
        config.setOption( BeliefSystemTypeOption.get( BeliefSystemType.JTMS.toString() ) );

        assertEquals( BeliefSystemTypeOption.get( BeliefSystemType.JTMS.toString() ),
                config.getOption( BeliefSystemTypeOption.class ) );

        // checking the string based getProperty() method
        assertEquals( BeliefSystemType.JTMS.getId(),
                config.getProperty( BeliefSystemTypeOption.PROPERTY_NAME ) );

        // setting the options using the string based setProperty() method
        config.setProperty( BeliefSystemTypeOption.PROPERTY_NAME,
                BeliefSystemType.DEFEASIBLE.getId() );

        // checking the type safe getOption() method
        assertEquals( BeliefSystemTypeOption.get( BeliefSystemType.DEFEASIBLE.getId() ),
                config.getOption( BeliefSystemTypeOption.class ) );
        // checking the string based getProperty() method
        assertEquals( BeliefSystemType.DEFEASIBLE.getId(),
                config.getProperty( BeliefSystemTypeOption.PROPERTY_NAME ) );
    }














}
