package com.shareniu.ch15;

import com.shareniu.BaseTest;
import com.shareniu.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

public class AppTest extends BaseTest {
    @Test
    public void testLockOnActive(){
        //
        KieSession kieSession = this.getKieSessionBySessionName("salience-rules");

        Person p = new Person();
        p.setAge(19);
        kieSession.insert(p);
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
