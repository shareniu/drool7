package com.shareniu.ch5;

import com.shareniu.BaseTest;
import com.shareniu.model.Person;
import org.junit.Test;
import org.kie.api.runtime.StatelessKieSession;

import java.util.ArrayList;
import java.util.List;

public class StatelessSessionTest extends BaseTest {


    @Test
    public  void  statelessSessionTest(){
        StatelessKieSession statelessKieSession = getStatelessKieSession();

        List<Person> list = new ArrayList<>();
        Person person=new Person();
        person.setAge(100);
        list.add(person);
        Person p2=new Person();
        p2.setAge(80);
        list.add(p2);
        statelessKieSession.execute(list);

    }
}
