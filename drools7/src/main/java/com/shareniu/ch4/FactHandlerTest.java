package com.shareniu.ch4;

import com.shareniu.BaseTest;
import com.shareniu.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

public class FactHandlerTest extends BaseTest {
    @Test
    public void testFactHandler() {

        KieSession kieSession = this.getKieSession("fact-handler-group");

        Person person=new Person();
        person.setAge(81);

        FactHandle factHandle = kieSession.insert(person);

        System.out.println(factHandle.toExternalForm());

        int count = kieSession.fireAllRules();
        System.out.println("Fires " + count + " rules!");

        person.setAge(1);
        kieSession.getAgenda().getAgendaGroup("fact-handler-group").setFocus();
        kieSession.update(factHandle, person);

        count = kieSession.fireAllRules();

        System.out.println("Fires " + count + " rules!");


        person.setAge(82);
        kieSession.getAgenda().getAgendaGroup("fact-handler-group").setFocus();
        kieSession.update(factHandle, person);

        count = kieSession.fireAllRules();

        System.out.println("Fires " + count + " rules!");
        kieSession.dispose();



    }

}
