package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.kie.builder.impl.*;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;

import java.util.Collection;

public class KieProjectTest extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieRepositoryImpl repository = (KieRepositoryImpl) kieServices.getRepository();
        System.out.println("repository:"+repository);
        ReleaseId defaultReleaseId = repository.getDefaultReleaseId();
        System.out.println("defaultReleaseId:"+defaultReleaseId);
        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);
        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();
        //默认返回null
        ReleaseId gav = kieProject.getGAV();
       // System.out.println("gav:"+gav.getArtifactId()+","+gav.getGroupId()+","+gav.getVersion());
        //默认返回0
        long creationTimestamp = kieProject.getCreationTimestamp();
        System.out.println("kieProject.getCreationTimestamp():"+creationTimestamp);
        Collection<String> kieBaseNames = kieProject.getKieBaseNames();

        // <kbase name="stateless-rules" packages="com.stateless">  name没有设置则会生成一个
        System.out.println("kieBaseNames:"+kieBaseNames);
        for (String kBaseName :kieBaseNames){
            System.out.println("############################################################");
            System.out.println("kBaseName:"+kBaseName);
            InternalKieModule kieModuleForKBase = kieProject.getKieModuleForKBase(kBaseName);
            System.out.println("getFileNames,"+kieModuleForKBase.getFileNames());
            System.out.println("getKieModuleModel,"+kieModuleForKBase.getKieModuleModel());
            System.out.println("getFile,"+kieModuleForKBase.getFile());
            System.out.println("getCreationTimestamp,"+kieModuleForKBase.getCreationTimestamp());
            System.out.println("getKieDependencies,"+kieModuleForKBase.getKieDependencies());
            System.out.println("getKnowledgeBuilderForKieBase"+kieModuleForKBase.getKnowledgeBuilderForKieBase(kBaseName));
            // readBytesFromInputStream(new FileInputStream(file));
            //java.lang.RuntimeException: java.io.FileNotFoundException: /Users/shareniu/workspace/drool7/drools7/target/classes (Is a directory)
            //byte[] bytes = kieModuleForKBase.getBytes();
            //getBytes()工具类
            //kieModuleForKBase.getBytes(t)

            kieModuleForKBase.getKnowledgePackagesForKieBase(kBaseName);
            System.out.println("############################################################");
        }

    }
}
