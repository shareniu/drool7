package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderConfigurationImpl;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.compiler.rule.builder.DroolsCompilerComponentFactory;
import org.drools.core.RuleBaseConfiguration;
import org.drools.core.definitions.InternalKnowledgePackage;
import org.drools.core.factmodel.ClassBuilderFactory;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.drools.core.rule.TypeDeclaration;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.definition.KiePackage;
import org.kie.internal.builder.conf.KBuilderSeverityOption;

import java.io.File;
import java.util.Collection;
import java.util.Set;

/**
 *
 *
 *
 *
 *
 *drools.dump.dir 属性配置之后
 * knowledgeBuilderConfiguration.getDumpDir()可以获取到
 *
 *KBuilderSeverityOption   drools.kbuilder.severity.
 *
 * drools.defaultPackageName可以设置 默认是defaultpkg
 *
 * drools.parser.processStringEscapes 配置忽略大小写
 *
 */
public class KnowledgeBuilderConfigurationImpInit7 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();
        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");
        System.out.println("knowledgeBuilder:"+knowledgeBuilder);

        KnowledgeBuilderConfigurationImpl knowledgeBuilderConfiguration = knowledgeBuilder.getBuilderConfiguration();
        File dumpDir = knowledgeBuilderConfiguration.getDumpDir();
        System.out.println("getDumpDir:"+dumpDir);

        //KBuilderSeverityOption   drools.kbuilder.severity.
        Set<String> optionKeys = knowledgeBuilderConfiguration.getOptionKeys(KBuilderSeverityOption.class);
        System.out.println("KBuilderSeverityOption:"+optionKeys);

        DroolsCompilerComponentFactory componentFactory = knowledgeBuilderConfiguration.getComponentFactory();
        System.out.println("componentFactory:"+componentFactory);

        ClassBuilderFactory classBuilderFactory = knowledgeBuilderConfiguration.getClassBuilderFactory();
        System.out.println("classBuilderFactory:"+classBuilderFactory);

        Set<String> transitiveIncludes = kieProject.getTransitiveIncludes("stateless-rules");
        System.out.println("transitiveIncludes:"+transitiveIncludes);
        InternalKieModule kieModule = kieProject.getKieModuleForKBase("stateless-rules");
        Collection<String> fileNames = kieModule.getFileNames();
        System.out.println("fileNames:"+fileNames);

    }
}
