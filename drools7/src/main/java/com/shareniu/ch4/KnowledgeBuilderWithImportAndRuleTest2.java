package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.compiler.kie.builder.impl.KieRepositoryImpl;
import org.drools.core.definitions.InternalKnowledgePackage;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.drools.core.rule.ImportDeclaration;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.definition.KieDefinition;
import org.kie.api.definition.rule.Rule;
import org.kie.internal.builder.KnowledgeBuilder;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 必须调用 KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");方法
 * 才可以通过kieProject.getKieModuleForKBase。.getKnowledgeBuilderForKieBase("stateless-rules");获取到KnowledgeBuilder
 *
 * kieContainer.getKieBase方法才开始创建获取到KnowledgeBuilder
 *    包生成策略：
 *    com.shareniu.test 会生成com.shareniu包
 *     com.shareniu.model.Person
 *  * com.shareniu.model.Goods
 *  会生成一个com.shareniu.model包
 *  规则文件中的 package  com.rules 会生成一个包
 *   /**
 *          * 规则文件中的package  com.rules
 *          * internalKnowledgePackage.getRules() 才有值
 *          *  每一个import对应一个ImportDeclaration对象
 *             package com.stateless对应一个com.stateless.* ImportDeclaration对象
 *
 *
 *          * package com.stateless
 *          *
 *          * import  com.shareniu.model.Person
 *          * import  com.shareniu.model.Goods
 *          * import  com.shareniu.test.A
 *          * rule "test-stateless"
 *          * when
 *          *     $p : A()
 *          * then
 *          *     System.out.println($p.getAge());
 *          *  end
 *          *
 *          *
 *
 *          ######start imports########
 *
 *
 * key:com.stateless.*
 * value:org.drools.core.rule.ImportDeclaration@f99f818
 * getTarget,com.stateless.*
 * getClass,class org.drools.core.rule.ImportDeclaration
 * ######end imports########
 *
 *
 * ######start imports########
 *
 *
 * key:com.shareniu.model.Goods
 * value:org.drools.core.rule.ImportDeclaration@aa7f330a
 * getTarget,com.shareniu.model.Goods
 * getClass,class org.drools.core.rule.ImportDeclaration
 * ######end imports########
 *
 *
 * ######start imports########
 * key:com.shareniu.model.Person
 * value:org.drools.core.rule.ImportDeclaration@b437727f
 * getTarget,com.shareniu.model.Person
 * getClass,class org.drools.core.rule.ImportDeclaration
 * ######end imports########
 *
 *
 * ######start imports########
 *
 *
 * key:com.shareniu.test.A
 * value:org.drools.core.rule.ImportDeclaration@ddf43b0a
 * getTarget,com.shareniu.test.A
 * getClass,class org.drools.core.rule.ImportDeclaration
 * ######end imports########
 *
 *          *######start rules########
 *          * getName(),test-stateless
 *          * getMetaData(),{}
 *          * getPackageName(),com.stateless
 *          * getId(),test-stateless
 *          * getNamespace(),com.stateless
 *          * getClass(),class org.drools.core.definitions.rule.impl.RuleImpl
 *          * getDeclaringClass(),class org.kie.api.definition.KieDefinition$KnowledgeType
 *          * ######end rules########

 */
public class KnowledgeBuilderWithImportAndRuleTest2 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);
        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();

        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        System.out.println(kieBase);

        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");
        System.out.println("knowledgeBuilder:"+knowledgeBuilder);

        InternalKnowledgePackage[] packages = knowledgeBuilder.getPackages();
        for (InternalKnowledgePackage internalKnowledgePackage:packages){
            System.out.println("################################################");
            System.out.println("\n");
            System.out.println("名称"+internalKnowledgePackage.getName());
            System.out.println("getTypeDeclarations:"+internalKnowledgePackage.getTypeDeclarations());
            Map<String, ImportDeclaration> imports = internalKnowledgePackage.getImports();
            System.out.println("getImports:"+imports);
            Set<Map.Entry<String, ImportDeclaration>> entries = imports.entrySet();
            for (Map.Entry<String, ImportDeclaration> entry :entries){
                System.out.println("######start imports########");
                System.out.println("\n");
                String key = entry.getKey();
                ImportDeclaration value = entry.getValue();
                System.out.println("key:"+key);
                System.out.println("value:"+value);
                System.out.println("getTarget,"+value.getTarget());
                System.out.println("getClass,"+value.getClass());

                System.out.println("######end imports########");
                System.out.println("\n");
            }

            System.out.println("getRules:"+internalKnowledgePackage.getRules());
            Collection<Rule> rules = internalKnowledgePackage.getRules();
            for (Rule rule:rules){
                System.out.println("######start rules########");
                System.out.println("\n");
                System.out.println("getName(),"+rule.getName());
                System.out.println("getMetaData(),"+rule.getMetaData());
                System.out.println("getPackageName(),"+rule.getPackageName());
                System.out.println("getId(),"+rule.getId());
                System.out.println("getNamespace(),"+rule.getNamespace());
                System.out.println("getClass(),"+rule.getClass());
                KieDefinition.KnowledgeType knowledgeType = rule.getKnowledgeType();
                System.out.println("getDeclaringClass(),"+knowledgeType.getDeclaringClass());
                System.out.println("######end rules########");
                System.out.println("\n");
            }
            System.out.println("\n");
            System.out.println("################################################");
        }


    }
}
