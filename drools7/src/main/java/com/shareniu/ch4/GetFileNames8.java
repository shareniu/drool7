package com.shareniu.ch4;

import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.junit.Test;
import org.kie.api.KieServices;

import java.util.Collection;

public class GetFileNames8 {

    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();
        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");


        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        InternalKieModule kieModule = kieProject.getKieModuleForKBase("stateless-rules");
        Collection<String> fileNames = kieModule.getFileNames();
        System.out.println("fileNames:"+fileNames);

    }
}
