package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderConfigurationImpl;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.compiler.PackageRegistry;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.base.evaluators.EvaluatorDefinition;
import org.drools.core.base.evaluators.EvaluatorRegistry;
import org.drools.core.base.evaluators.Operator;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.rule.AccumulateFunction;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 *drools.evaluator.coincides = org.drools.core.base.evaluators.CoincidesEvaluatorDefinition
 * drools.evaluator.before = org.drools.core.base.evaluators.BeforeEvaluatorDefinition
 * drools.evaluator.after = org.drools.core.base.evaluators.AfterEvaluatorDefinition
 * drools.evaluator.meets = org.drools.core.base.evaluators.MeetsEvaluatorDefinition
 * drools.evaluator.metby = org.drools.core.base.evaluators.MetByEvaluatorDefinition
 * drools.evaluator.overlaps = org.drools.core.base.evaluators.OverlapsEvaluatorDefinition
 * drools.evaluator.overlappedby = org.drools.core.base.evaluators.OverlappedByEvaluatorDefinition
 * drools.evaluator.during = org.drools.core.base.evaluators.DuringEvaluatorDefinition
 * drools.evaluator.includes = org.drools.core.base.evaluators.IncludesEvaluatorDefinition
 * drools.evaluator.starts = org.drools.core.base.evaluators.StartsEvaluatorDefinition
 * drools.evaluator.startedby = org.drools.core.base.evaluators.StartedByEvaluatorDefinition
 * drools.evaluator.finishes = org.drools.core.base.evaluators.FinishesEvaluatorDefinition
 * drools.evaluator.finishedby = org.drools.core.base.evaluators.FinishedByEvaluatorDefinition
 * drools.evaluator.set = org.drools.core.base.evaluators.SetEvaluatorsDefinition
 * drools.evaluator.matches = org.drools.core.base.evaluators.MatchesEvaluatorsDefinition
 * drools.evaluator.soundslike = org.drools.core.base.evaluators.SoundslikeEvaluatorsDefinition
 *
 * 默认是20个类
 */
public class KnowledgeBuilderConfigEvaluator5 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();

        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");

        System.out.println("knowledgeBuilder:"+knowledgeBuilder);

        KnowledgeBuilderConfigurationImpl builderConfiguration = knowledgeBuilder.getBuilderConfiguration();

        EvaluatorRegistry evaluatorRegistry = builderConfiguration.getEvaluatorRegistry();
        System.out.println("evaluatorRegistry"+evaluatorRegistry);
        Set<String> strings = evaluatorRegistry.keySet();
        System.out.println("size,"+strings.size());
        System.out.println("evaluatorRegistry.keySet():"+strings);

        EvaluatorDefinition evaluatorDefinition = builderConfiguration.getEvaluatorRegistry().getEvaluatorDefinition(Operator.MATCHES);
        System.out.println("evaluatorDefinition:"+evaluatorDefinition);

        EvaluatorDefinition.Target target = evaluatorDefinition.getTarget();
        System.out.println("target,"+target);

        String[] evaluatorIds = evaluatorDefinition.getEvaluatorIds();
        System.out.println("evaluatorIds,"+evaluatorIds);


        EvaluatorDefinition before = builderConfiguration.getEvaluatorRegistry().getEvaluatorDefinition("before");
        System.out.println("before:"+before);
    }
}
