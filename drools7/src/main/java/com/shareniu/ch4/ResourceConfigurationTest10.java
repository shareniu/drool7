package com.shareniu.ch4;

import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.kie.api.KieServices;
import org.kie.api.io.ResourceConfiguration;
import org.kie.api.io.ResourceType;

import java.util.Collection;

public class ResourceConfigurationTest10 {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();
        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");

       //需要配置kie.resource.conf.class属性
        ResourceConfiguration resourceConfiguration = kModule.getResourceConfiguration("com.rules/1");
        System.out.println(resourceConfiguration);

    }
}
