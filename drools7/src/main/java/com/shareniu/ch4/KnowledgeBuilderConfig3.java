package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderConfigurationImpl;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.compiler.DialectConfiguration;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.base.evaluators.EvaluatorRegistry;
import org.drools.core.definitions.InternalKnowledgePackage;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.drools.core.rule.ImportDeclaration;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.definition.KieDefinition;
import org.kie.api.definition.rule.Rule;
import org.kie.internal.builder.conf.LanguageLevelOption;
import org.kie.internal.utils.ChainedProperties;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 drools.dialect.default = java
 drools.dialect.java = org.drools.compiler.rule.builder.dialect.java.JavaDialectConfiguration
 drools.dialect.java.compiler = ECLIPSE

 drools.dialect.mvel = org.drools.compiler.rule.builder.dialect.mvel.MVELDialectConfiguration
 drools.dialect.mvel.strict = true
 drools.dialect.mvel.langLevel = 4
 */
public class KnowledgeBuilderConfig3 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();

        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");

        System.out.println("knowledgeBuilder:"+knowledgeBuilder);

        KnowledgeBuilderConfigurationImpl builderConfiguration = knowledgeBuilder.getBuilderConfiguration();
        ChainedProperties chainedProperties = builderConfiguration.getChainedProperties();
        System.out.println("chainedProperties:"+chainedProperties);

        String defaultDialect = builderConfiguration.getDefaultDialect();
        System.out.println("defaultDialect:"+defaultDialect);

        Collection<String> accumulateFunctionNames = builderConfiguration.getAccumulateFunctionNames();
        System.out.println("accumulateFunctionNames:"+accumulateFunctionNames);

        String defaultPackageName = builderConfiguration.getDefaultPackageName();
        System.out.println("defaultPackageName:"+defaultPackageName);

        LanguageLevelOption languageLevel = builderConfiguration.getLanguageLevel();
        System.out.println("languageLevel:"+languageLevel);

        DialectConfiguration java = builderConfiguration.getDialectConfiguration("java");
        System.out.println("java:"+java);
        DialectConfiguration mvel = builderConfiguration.getDialectConfiguration("mvel");
        System.out.println("mvel:"+mvel);

        EvaluatorRegistry evaluatorRegistry = builderConfiguration.getEvaluatorRegistry();
        System.out.println("evaluatorRegistry"+evaluatorRegistry);
        Set<String> strings = evaluatorRegistry.keySet();
        System.out.println("evaluatorRegistry.keySet():"+strings);
    }
}
