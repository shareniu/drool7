package com.shareniu.ch4;

import org.kie.api.io.ResourceType;

public class ResourceTypeTest9 {

    public static void main(String[] args) {
        ResourceType resourceType = ResourceType.determineResourceType("1.bpmn");
        System.out.println(resourceType.getDefaultExtension());
        System.out.println(resourceType.getDefaultPath());
        System.out.println(resourceType.getDescription());
        System.out.println(resourceType.getName());
    }
}
