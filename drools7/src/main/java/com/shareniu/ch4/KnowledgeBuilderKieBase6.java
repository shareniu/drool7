package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderConfigurationImpl;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.RuleBaseConfiguration;
import org.drools.core.base.evaluators.EvaluatorDefinition;
import org.drools.core.base.evaluators.EvaluatorRegistry;
import org.drools.core.base.evaluators.Operator;
import org.drools.core.definitions.InternalKnowledgePackage;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.drools.core.rule.TypeDeclaration;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.definition.KiePackage;

import java.util.Collection;
import java.util.Set;

/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
public class KnowledgeBuilderKieBase6 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();

        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        String containerId = kieBase.getContainerId();
        System.out.println("containerId:"+containerId);

        Collection<KiePackage> kiePackages = kieBase.getKiePackages();
        System.out.println("kiePackages:"+kiePackages);

        String id = kieBase.getId();
        System.out.println("id:"+id);

        InternalKnowledgePackage[] packages = kieBase.getPackages();
        System.out.println("packages:"+packages);

        Collection<TypeDeclaration> typeDeclarations = kieBase.getTypeDeclarations();
        System.out.println("typeDeclarations:"+typeDeclarations);

        RuleBaseConfiguration configuration = kieBase.getConfiguration();
        System.out.println("configuration:"+configuration);

    }
}
