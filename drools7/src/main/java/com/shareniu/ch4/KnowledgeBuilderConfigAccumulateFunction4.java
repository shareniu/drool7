package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderConfigurationImpl;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.compiler.DialectConfiguration;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.base.accumulators.IntegerSumAccumulateFunction;
import org.drools.core.base.evaluators.EvaluatorRegistry;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.rule.AccumulateFunction;
import org.kie.internal.builder.conf.LanguageLevelOption;
import org.kie.internal.utils.ChainedProperties;

import java.util.Collection;
import java.util.Set;

/**
 * drools.accumulate.function.max = org.drools.core.base.accumulators.MaxAccumulateFunction
 * drools.accumulate.function.maxN = org.drools.core.base.accumulators.NumericMaxAccumulateFunction
 * drools.accumulate.function.min = org.drools.core.base.accumulators.MinAccumulateFunction
 * drools.accumulate.function.minN = org.drools.core.base.accumulators.NumericMinAccumulateFunction
 * drools.accumulate.function.count = org.drools.core.base.accumulators.CountAccumulateFunction
 * drools.accumulate.function.collectList = org.drools.core.base.accumulators.CollectListAccumulateFunction
 * drools.accumulate.function.collectSet = org.drools.core.base.accumulators.CollectSetAccumulateFunction
 * drools.accumulate.function.average = org.drools.core.base.accumulators.AverageAccumulateFunction
 * drools.accumulate.function.averageBD = org.drools.core.base.accumulators.BigDecimalAverageAccumulateFunction
 * drools.accumulate.function.sum = org.drools.core.base.accumulators.SumAccumulateFunction
 * drools.accumulate.function.sumI = org.drools.core.base.accumulators.IntegerSumAccumulateFunction
 * drools.accumulate.function.sumL = org.drools.core.base.accumulators.LongSumAccumulateFunction
 * drools.accumulate.function.sumBI = org.drools.core.base.accumulators.BigIntegerSumAccumulateFunction
 * drools.accumulate.function.sumBD = org.drools.core.base.accumulators.BigDecimalSumAccumulateFunction
 * drools.accumulate.function.variance = org.drools.core.base.accumulators.VarianceAccumulateFunction
 * drools.accumulate.function.standardDeviation = org.drools.core.base.accumulators.StandardDeviationAccumulateFunction
 *
 * 默认是16个类
 */
public class KnowledgeBuilderConfigAccumulateFunction4 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);

        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();

        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");

        System.out.println("knowledgeBuilder:"+knowledgeBuilder);

        KnowledgeBuilderConfigurationImpl builderConfiguration = knowledgeBuilder.getBuilderConfiguration();

        Collection<String> accumulateFunctionNames = builderConfiguration.getAccumulateFunctionNames();
        System.out.println("accumulateFunctionNames size:"+accumulateFunctionNames.size());
        System.out.println("accumulateFunctionNames:"+accumulateFunctionNames);

        for(String accumulateFunctionName:accumulateFunctionNames){
            AccumulateFunction accumulateFunction = builderConfiguration.getAccumulateFunction(accumulateFunctionName);
            System.out.println("accumulateFunction:"+accumulateFunction);
        }
//        IntegerSumAccumulateFunction  integerSumAccumulateFunction=new IntegerSumAccumulateFunction();
//         IntegerSumAccumulateFunction.SumData context = integerSumAccumulateFunction.createContext();
//        integerSumAccumulateFunction.init();
//        integerSumAccumulateFunction.accumulate(integerSumAccumulateFunction.createContext(),144);
//        Object result = integerSumAccumulateFunction.getResult(integerSumAccumulateFunction.createContext());
//        System.out.println(result);
    }
}
