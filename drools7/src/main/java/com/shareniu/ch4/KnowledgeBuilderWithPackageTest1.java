package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.builder.impl.KnowledgeBuilderConfigurationImpl;
import org.drools.compiler.builder.impl.KnowledgeBuilderImpl;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.core.definitions.InternalKnowledgePackage;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.drools.core.rule.ImportDeclaration;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.definition.KieDefinition;
import org.kie.api.definition.rule.Rule;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 必须调用 KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");方法
 * 才可以通过kieProject.getKieModuleForKBase。.getKnowledgeBuilderForKieBase("stateless-rules");获取到KnowledgeBuilder
 *
 * kieContainer.getKieBase方法才开始创建获取到KnowledgeBuilder
 *   InternalKnowledgePackage  包生成策略：
 *    com.shareniu.test 会生成com.shareniu包
 *     com.shareniu.model.Person
 *  * com.shareniu.model.Goods
 *  会生成一个com.shareniu.model包
 *      该包下面所有的在规则文件中使用到的类都会被存储下来。
 *     getTypeDeclarations:{
 *     Car=TypeDeclaration{typeName='Car', role=FACT, format=POJO, kind=CLASS, nature=DECLARATION},
 *     Goods=TypeDeclaration{typeName='Goods', role=FACT, format=POJO, kind=CLASS, nature=DECLARATION},
 *     Person=TypeDeclaration{typeName='Person', role=FACT, format=POJO, kind=CLASS, nature=DECLARATION}}
 *  规则文件中的 package  com.rules 会生成一个包(com.rules)
 *
 *
 *

 */
public class KnowledgeBuilderWithPackageTest1 extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);
        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();

        Collection<String> kieBaseNames = kieProject.getKieBaseNames();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        System.out.println(kieBase);

        InternalKieModule kModule = kieProject.getKieModuleForKBase("stateless-rules" );
        KnowledgeBuilderImpl knowledgeBuilder = (KnowledgeBuilderImpl) kModule.getKnowledgeBuilderForKieBase("stateless-rules");
        System.out.println("knowledgeBuilder:"+knowledgeBuilder);
        InternalKnowledgePackage[] packages = knowledgeBuilder.getPackages();
        for (InternalKnowledgePackage internalKnowledgePackage:packages){
            System.out.println("################################################");
            System.out.println("名称"+internalKnowledgePackage.getName());
            System.out.println("getTypeDeclarations:"+internalKnowledgePackage.getTypeDeclarations());
        }




    }
}
