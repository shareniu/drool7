package com.shareniu.ch4;

import com.shareniu.BaseTest;
import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.compiler.kie.builder.impl.KieRepositoryImpl;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;

public class CodingTest extends BaseTest {
    @Test
    public void testFactHandler() {
        KieServices kieServices = KieServices.get();

        KieRepositoryImpl repository = (KieRepositoryImpl) kieServices.getRepository();
        System.out.println("repository:"+repository);
        ReleaseId defaultReleaseId = repository.getDefaultReleaseId();
        System.out.println("defaultReleaseId:"+defaultReleaseId);
        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        System.out.println("kieContainer######################:"+kieContainer);
        ReleaseId releaseId = kieContainer.getReleaseId();
        System.out.println(releaseId);
        ClasspathKieProject kieProject = (ClasspathKieProject) kieContainer.getKieProject();
        System.out.println("kieProject:"+kieProject);
        long creationTimestamp = kieProject.getCreationTimestamp();
        System.out.println("creationTimestamp:"+creationTimestamp);
        InternalKieModule kieModuleForKBase = kieProject.getKieModuleForKBase("rules");
        ReleaseId releaseId1 = kieModuleForKBase.getReleaseId();
        System.out.println("releaseId1:"+releaseId1);
        /**
         * kieModuleForKBase:FileKieModule[releaseId=com.shareniu:drools7:1.0-SNAPSHOT,file=/Users/shareniu/software/drools/drools/drools7/target/classes]
         */
        System.out.println("kieModuleForKBase:"+kieModuleForKBase);
        /**
         * kieModuleForKBase getFileNames:[com.activationGroup/activationGroup.drl, com.lhs/lhs.drl, com.stateless/stateless.drl, com.timerTest/timer.drl, com.ruleFlowGroup/2.drl, com.ruleFlowGroup/1.drl, com.rules/Drools7ApiTest.drl, com.rules/FactHandlerTest.drl, com.salienceTest/1.drl, META-INF/kmodule.xml, com.no-loop/no-loop.drl, com.lockOnActive/1.drl, readme.text, com.dateEffective/dateEffective.drl, com/shareniu/ch12/LoopTest.class, com/shareniu/ch15/AppTest.class, com/shareniu/ch14/AppTest.class, com/shareniu/ch13/AppTest.class, com/shareniu/ch3/Drools7ApiTest.class, com/shareniu/ch4/CodingTest.class, com/shareniu/ch4/FactHandlerTest.class, com/shareniu/ch5/StatelessSessionTest.class, com/shareniu/model/Car.class, com/shareniu/model/Person.class, com/shareniu/model/Server.class, com/shareniu/model/SubPerson.class, com/shareniu/model/Goods.class, com/shareniu/BaseTest.class, com/shareniu/ch17/TimerTest$1.class, com/shareniu/ch17/AutoFocusTest.class, com/shareniu/ch17/TimerTest.class, com/shareniu/ch17/ActivationGroupTest.class, com/shareniu/ch17/LhsTest.class,
         * com/shareniu/ch17/DateEffectiveTest.class, com.autoFocus/autoFocus.drl]
         */
        System.out.println("kieModuleForKBase getFileNames:"+kieModuleForKBase.getFileNames());
        /**
         * kieModuleForKBase getKieModuleModel:KieModuleModel [kbases={ruleFlowGroup-rules=KieBaseModelImpl
         * [name=ruleFlowGroup-rules, includes=[], packages=[com.ruleFlowGroup], equalsBehavior=IDENTITY,
         * eventProcessingMode=EventProcessingOption( cloud ), kSessions={ruleFlowGroup-rules=KieSessionModel
         * kSessions={all-rules=KieSessionModel [name=all-rules, clockType=ClockTypeOption( realtime )]}]}]
         */
        System.out.println("kieModuleForKBase getKieModuleModel:"+kieModuleForKBase.getKieModuleModel());
    }
}
