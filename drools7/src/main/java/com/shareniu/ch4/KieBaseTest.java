package com.shareniu.ch4;

import org.drools.compiler.kie.builder.impl.ClasspathKieProject;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.drools.compiler.kie.builder.impl.KieRepositoryImpl;
import org.drools.core.definitions.impl.KnowledgePackageImpl;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.drools.core.rule.TypeDeclaration;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.definition.KiePackage;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.rule.AccumulateFunction;

import java.util.Collection;
import java.util.Map;

/**
 * kieContainer.newStatelessKieSession 所做的事情
 *  <ksession name="stateless-rules1" type="stateless"/>
 *  类型必须是stateless 否则直接报错
 *
 *  1、 创建createKieBase 填充KieContainerImpl对象中的kBases字段属性
 *  2、AbstractKieProject类中创建KnowledgeBuilderConfigurationImpl
 *     KnowledgeBuilderConfigurationImpl构造函数
 *                 构造ChainedProperties类中存储一系列的属性
 *                      System.getProperties()
 *                      META-INF/kie.properties.conf（drools-core包）
 *                      META-INF/kie.default.properties.conf（drools-core包 ） 存储于defaultProps属性中
 *                     校验是否存在drools.dialect.java属性
 *                     设置一系列内置属性
 *
 *                   反射实例化 JavaDialectConfiguration类并调用init方法
 *                   获取drools.dialect.java.compiler属性，没有则当做ECLIPSE
 *                   获取drools.dialect.java.compiler.lnglevel ，没有则使用System.getProperty("java.version")
 *                   默认是"1.5", "1.6", "1.7", "1.8", "9"
 *
 *                   反射实例化 org.drools.compiler.rule.builder.dialect.mvel.MVELDialectConfiguration  mvel
 *  *                   获取drools.dialect.mvel.strict属性，没有则当做true
 *  *                   获取drools.dialect.mvel.langLevel ，没有则使用4
 *  *                   默认是"1.5", "1.6", "1.7", "1.8", "9"
 *                 两种方言最终存储于KnowledgeBuilderConfigurationImpl类的dialectConfigurations属性中
 *                构造AccumulateFunction
 *                构造drools.kbuilder.severity
 *                设置drools.parser.processStringEscape  默认是true
 *                设置drools.defaultPackageName          默认是defaultpkg
 *                实例化DroolsCompilerComponentFactory
 *                设置ClassBuilderFactory= new ClassBuilderFactory();
 *
 *
 *          实例化RuleBaseConfiguration类
 *
 *     kieBase.getKiePackages();包括drl规则文件的package com.stateless以及import并且在规则中使用的包
 */
public class KieBaseTest {


    @Test
    public void testFactHandler() {
        System.setProperty("shareniu","shareniu");
        KieServices kieServices = KieServices.get();

        KieRepositoryImpl repository = (KieRepositoryImpl) kieServices.getRepository();
        System.out.println("repository:" + repository);
        ReleaseId defaultReleaseId = repository.getDefaultReleaseId();
        System.out.println("defaultReleaseId:" + defaultReleaseId);
        KieContainerImpl kieContainer = (KieContainerImpl) kieServices.getKieClasspathContainer();
        KnowledgeBaseImpl kieBase = (KnowledgeBaseImpl) kieContainer.getKieBase("stateless-rules");
        System.out.println(kieBase);

        String containerId = kieBase.getContainerId();
        System.out.println("containerId:"+containerId);

        Collection<KiePackage> kiePackages = kieBase.getKiePackages();

        for(KiePackage kiePackage:kiePackages){
            System.out.println("getName:"+kiePackage.getName());
            KnowledgePackageImpl knowledgePackage= (KnowledgePackageImpl) kiePackage;
            Map<String, TypeDeclaration> typeDeclarations = knowledgePackage.getTypeDeclarations();
            System.out.println(typeDeclarations);
        }
        StatelessKieSession kieSession = kieContainer.newStatelessKieSession("stateless-rules1");

    }
}
