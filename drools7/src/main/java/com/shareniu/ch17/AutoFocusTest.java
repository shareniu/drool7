package com.shareniu.ch17;

import com.shareniu.BaseTest;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

public class AutoFocusTest extends BaseTest {

    @Test
    public void testAutoFocus(){
        KieSession kieSession = this.getKieSessionBySessionName("autoFocus-rules");
        kieSession.getAgenda().getAgendaGroup("auto-focus-2").setFocus();
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
