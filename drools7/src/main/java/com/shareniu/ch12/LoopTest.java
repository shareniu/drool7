package com.shareniu.ch12;

import com.shareniu.BaseTest;
import com.shareniu.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

public class LoopTest extends BaseTest {
    @Test
    public void testNoLoop() {
        //no-loop false 默认的
        KieSession kieSession = this.getKieSessionBySessionName("no-loop-rules");

        Person person=new Person();
        person.setAge(5);
        kieSession.insert(person);
        kieSession.fireAllRules();
        kieSession.dispose();
    }

}
