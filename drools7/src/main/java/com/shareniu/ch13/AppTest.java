package com.shareniu.ch13;

import com.shareniu.BaseTest;
import com.shareniu.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

public class AppTest extends BaseTest {
    @Test
    public void testNoLoop() {
        //ruleflow-group  中的规则只会被触发一次，不会被触发多次  好像没有先后顺序
        KieSession kieSession = this.getKieSessionBySessionName("ruleFlowGroup-rules");

        kieSession.getAgenda().getAgendaGroup("r2").setFocus();
        kieSession.getAgenda().getAgendaGroup("r1").setFocus();
        kieSession.fireAllRules();

        kieSession.getAgenda().getAgendaGroup("r2").setFocus();
        kieSession.getAgenda().getAgendaGroup("r1").setFocus();
        kieSession.fireAllRules();


        kieSession.dispose();
    }

}
