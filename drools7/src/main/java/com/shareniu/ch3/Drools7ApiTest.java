package com.shareniu.ch3;


import com.shareniu.BaseTest;
import com.shareniu.model.Car;
import com.shareniu.model.Person;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

public class Drools7ApiTest extends BaseTest {
    @Test
    public void testDrools7Api() {
        KieSession kieSession = getKieSession("test-drools7");


        Person p1 = new Person();
        p1.setAge(50);
        Car c1 = new Car();
        c1.setPerson(p1);

        Person p2 = new Person();
        p2.setAge(70);
        Car c2 = new Car();
        c2.setPerson(p2);

        kieSession.insert(c1);
        kieSession.insert(c2);

        int i = kieSession.fireAllRules();
        System.out.println(i);
        kieSession.dispose();
    }

}
